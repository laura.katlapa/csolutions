<?php


user::id(0);


$login_error = '';
if (
	array_key_exists('user', $_POST)
	and array_key_exists('pass', $_POST)
) {
	if (!filter_var($_POST['user'], FILTER_VALIDATE_EMAIL)) {
        $login_error = 'Nepareiza epasta adrese!';
    }
    elseif (empty($_POST['pass'])) {
	    $login_error = 'Lūdzu ievadiet paroli!';
    }
    elseif (!user::login($_POST['user'], $_POST['pass'])) {
        $login_error = 'Epasta un paroles kombinācija nav pareiza!';
	}
    else {
	    core::redirect(); // success!
    }
}


?>
<section>
	<?= ($login_error ? '<p style="color:red">'.$login_error.'</p>' : '') ?>
	<nav>
		<h3>Autorizācija</h3>
		<form action="" method="post">
			<label><input name="user" required type="email" placeholder="Lietotājs"></label>
			<label><input name="pass" required type="password" placeholder="Parole"></label>
			<input type="submit" value="Login">
		</form>
	</nav>
</section>
