<?php


// log in/out
$user_id = user::id();
if (!$user_id) {
    core::redirect('login');
}


$form = [
	'id' => '0',
	'name' => '',
	'count' => '',
	'price' => '',
];


// posted?
$is_admin = in_array('admin', user::role($user_id)); // TODO: user::is_admin()
if ($is_admin) {
	if (
		array_key_exists('id', $_POST)
		and array_key_exists('name', $_POST)
		and array_key_exists('count', $_POST)
		and array_key_exists('price', $_POST)
	) {
        // TODO: replace increases AUTO_INCREMENT on update
		core::$db_sql->prepare('
            REPLACE INTO `product` (id, name, count, price)
            VALUES (:id, :name, :count, :price)
        ')->execute([
            'id' => $_POST['id'] ?: NULL,
			'name' => $_POST['name'],
			'count' => $_POST['count'],
			'price' => $_POST['price'],
		]);
        $id = core::$db_sql->lastInsertId();

		core::$db_sql->prepare('
            INSERT INTO `product_log` (id, name, count, price, user_id, time)
            VALUES (:id, :name, :count, :price, :user_id, :time)
        ')->execute([
            'id' => $id,
			'name' => $_POST['name'],
			'count' => $_POST['count'],
			'price' => $_POST['price'],
			'user_id' => $user_id,
			'time' => time(),
		]);

		core::redirect();
	}

    if (array_key_exists('delete', $_GET)) {
	    core::$db_sql->prepare('
            DELETE FROM `product`
            WHERE id = :id
        ')->execute([
            'id' => $_GET['delete'],
	    ]);

	    core::$db_sql->prepare('
            INSERT INTO `product_log` (id, name, count, price, user_id, time)
            VALUES (:id, :name, :count, :price, :user_id, :time)
        ')->execute([
            'id' => $_GET['delete'],
		    'name' => '',
		    'count' => 0,
		    'price' => 0,
		    'user_id' => $user_id,
		    'time' => time(),
	    ]);

	    core::redirect();
    }

    if (array_key_exists('update', $_GET)) {
        $q = core::$db_sql->prepare('
            SELECT id, name, count, price
            FROM `product`
            WHERE id = :id
        ');
        $q->execute([
            'id' => $_GET['update'],
        ]);

        if ($row = $q->fetch()) {
            $form = array_replace($form, $row);
        }
    }
}


// show products
?>
<section>
    <table>
        <tr>
            <th>#</th>
            <th>Prece nosaukums</th>
            <th>Vienību skaits</th>
            <th>Cena par vienu vienību</th>
            <th>Cena ar PVN</th>
        </tr>
        <?php
        $q = core::$db_sql->query('
            SELECT id, name, count, price
            FROM `product`
        ');
        $data = $q->fetchAll();
        if ($data) {
            foreach ($data as $row) {
                echo
                    '<tr>',
                    '<td>', $row['id'] ,'</td>',
                    '<td>', html($row['name']) ,'</td>',
                    '<td>', $row['count'] ,'</td>',
                    '<td>', number_format($row['price'], 2) ,'</td>',
                    '<td>', number_format(($row['count'] * $row['price']) * (1 + core::$conf['vat']), 2) ,'</td>',
                    ($is_admin ? '<td><a href="?update='.$row['id'].'">[update]</a> <a href="?delete='.$row['id'].'">[delete]</a></td>' : ''),
                    '</tr>';
            }
        }

        if ($is_admin) { // TODO: move form outside table?
	        ?>
            <form action="" method="post">
                <tr>
                    <td><input name="id" value="<?= html($form['id']) ?>" type="hidden"></td>
                    <td><input name="name" value="<?= html($form['name']) ?>"></td>
                    <td><input name="count" value="<?= html($form['count']) ?>" type="number"></td>
                    <td><input name="price" value="<?= html($form['price']) ?>"></td>
                    <td><input type="submit" value="<?= $form['id'] ? 'Saglabāt' : 'Pievienot' ?>"></td>
                </tr>
            </form>
	        <?php
        }
        ?>
    </table>
</section>
