<?php

/**
 * @var string $content
 */

$user_id = user::id();

?><!DOCTYPE html>
<html lang="en"><head>
    <meta charset="UTF-8">
    <title><?= $_SERVER['HTTP_HOST'] ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="initial-scale=1.0">
    <link rel="stylesheet" href="/assets/default.css">
</head><body>

<header>
    <?=
        $user_id
        ? '<div>'.user::name($user_id).' <a href="/login">[logout]</a></div>'
        : ''
    ?>
</header>

<main><?= $content ?></main>

<footer></footer>

<script src="/assets/jquery.min.js"></script>
<script src="/assets/default.js"></script>

</body></html>