CREATE TABLE `product` (
    `id` int(10) UNSIGNED NOT NULL,
    `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    `count` int(10) UNSIGNED NOT NULL,
    `price` decimal(9,4) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

ALTER TABLE `product`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `product`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;


CREATE TABLE `product_log` (
    `id` int(10) UNSIGNED NOT NULL,
    `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
    `count` int(10) UNSIGNED DEFAULT NULL,
    `price` decimal(9,4) UNSIGNED DEFAULT NULL,
    `user_id` int(10) UNSIGNED NOT NULL,
    `time` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;


CREATE TABLE `users` (
    `id` int(10) UNSIGNED NOT NULL,
    `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
    `role` set('admin') COLLATE ascii_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=ascii COLLATE=ascii_bin;

ALTER TABLE `users`
    ADD PRIMARY KEY (`id`);

ALTER TABLE `users`
    MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;


CREATE TABLE `users_auth` (
    `user_id` int(10) UNSIGNED NOT NULL,
    `uname` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
    `passw` varchar(255) CHARACTER SET ascii COLLATE ascii_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `users_auth`
    ADD UNIQUE KEY `uname` (`uname`);
