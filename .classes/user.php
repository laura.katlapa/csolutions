<?php


// TODO: cache user data and replace static funcs


class user {
	public static function id($set = NULL) {
		if ($set !== NULL) {
			$_SESSION['user'] = $set;
		}
		return (array_key_exists('user', $_SESSION) ? $_SESSION['user'] : 0);
	}


	public static function login($user, $pass) {
		$q = core::$db_sql->prepare('
			SELECT user_id
			FROM `users_auth`
			WHERE uname = :u AND passw = :p
		');
		$q->execute([
			'u' => $user,
			'p' => md5($pass), // TODO: password_verify()
		]);
		return ($u_id = $q->fetchColumn()) ? user::id($u_id) : user::id(0);
	}


    public static function name($id) {
        $q = core::$db_sql->prepare('
			SELECT name
			FROM `users`
			WHERE id = ?
		');
        $q->execute([ $id ]);
        return $q->fetchColumn();
    }


	public static function role($user_id) {
		$q = core::$db_sql->prepare('
			SELECT role
			FROM `users`
			WHERE id = :id
		');
		$q->execute([
			'id' => $user_id,
		]);
		return ($role = $q->fetchColumn()) ? explode(',', $role) : [];
	}
}
