<?php


class api {
    public static function products() {
	    // TODO: use human-readable from-till dates and mktime()?

		$where = $params = [];
		if (array_key_exists('from', $_GET)) {
			$where[] = 'time >= :from';
			$params['from'] = intval($_GET['from']);
		}
	    if (array_key_exists('till', $_GET)) {
		    $where[] = 'time <= :till';
		    $params['till'] = intval($_GET['till']);
	    }

	    $limit = 10;
	    if (array_key_exists('limit', $_GET)) {
			$limit = min(1000, max(1, intval($_GET['limit'])));
	    }

        $q = core::$db_sql->prepare('
            SELECT id, name, count, price, user_id, time
            FROM `product_log`
            '.($where ? 'WHERE '.implode(' AND ', $where) : '').'
            ORDER BY time DESC
            LIMIT '.$limit.' # cannot be passed normally
        ');
		$q->execute($params);

        return $q->fetchAll();
    }
}
