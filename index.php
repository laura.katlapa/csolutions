<?php


require '.system/core.php';


// basic router
$url = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
switch ($url) {
	case '/API/products/json':
		$data = api::products();
		header('content-type: application/json; charset=utf-8');
		echo json_encode($data);
		break;

	case '/API/products/print':
		$data = api::products();
		header('content-type: application/json; charset=utf-8');
		echo json_encode($data, JSON_PRETTY_PRINT);
		break;

	case '/':
		ob_start();
		require '.views/home.php';
		$content = ob_get_contents();
		ob_end_clean();
		require '.views/.html.php';
		break;

	case '/login':
		ob_start();
		require '.views/login.php';
		$content = ob_get_contents();
		ob_end_clean();
		require '.views/.html.php';
		break;

	default:
		http_response_code(404);
}
